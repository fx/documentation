# Group page

## Report a group

To report a group, you have to:

  1. click ⋅⋅⋅ button
  * click **Report** button:
  
    ![report group image](../../images/en/report-group.png)

  * [Optional but **recommended**] filling the report modal with a comment:
    ![groupe report modal image](../../images/en/report-group-modal.png)
