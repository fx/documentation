# How to deal with moderation

!!! info
    You have to be **moderator** or **administrator** to access this section

This section aims to explain how to deal with [comment](../../events/comment-event/#report-a-comment) and [event](../../events/event-action/#report) reports.

## Reports view

!!! note
    When someone reports a comment or event, moderators and administrators receive a notification email.

Reports are available by:

  1. clicking your avatar
  * clicking **Administration**
  * clicking **Reports** in **Moderation** section in the left sidebar menu

You can filter reports by **Open**, **Resolved** and **Closed** ones:

![new report view](../../images/en/moderator-reports-list-EN.png)

### Reported comments

When a new report is available, you can click on it to open actions view:

![report comments actions view annoted](../../images/en/moderator-report-comment-view-annoted-EN.png)

  1. **Mark as resolved** button: report accepted and processed
  * **Close** button: report rejected
  * Delete reported **event**
  * Comment filled by the account **that's report** the comment
  * Name of the **reported account** and comment reported
  * Delete **reported comment**

### Reported events

When a new report is available, you can click on it to open actions view:

![report avents actions view annoted](../../images/en/moderator-report-event-view-annoted-EN.png)

  1. **Mark as resolved** button: report accepted and processed
  * **Close** button: report rejected
  * Comment filled by the account **that's report** the event
  * Event reported
  * Delete the event

### Moderation discussion

Moderators can discuss about a report by using **Notes** section:

![report notes](../../images/en/report-notes.png)

## Moderation log

This section lists the actions that have been done by the moderators.
